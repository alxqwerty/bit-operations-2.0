﻿using System;
using System.Text;

namespace BitOperationsTask
{
    public static class NumbersExtension
    {
        public static int InsertNumberIntoAnother(int destinationNumber, int sourceNumber, int i, int j)
        {
            if (i == 32 || i == -1)
            {
                throw new ArgumentOutOfRangeException(nameof(i), "Value is out of range.");
            }

            if (i > j)
            {
                throw new ArgumentException($"Value of {i} isn't bigger than {j}.", nameof(i));
            }

            if (j == 32)
            {
                throw new ArgumentOutOfRangeException(nameof(j), "Value is out of range.");
            }

            int result = 0;

            string convertedDestinationNumber = FromNumberToBitConverter(destinationNumber);
            char[] destinationArray = convertedDestinationNumber.ToCharArray();
            Array.Reverse(destinationArray);

            string convertedSourceNumber = FromNumberToBitConverter(sourceNumber);
            char[] sourceArray = convertedSourceNumber.ToCharArray();
            Array.Reverse(sourceArray);

            for (int k = 0; i <= j; k++, i++)
            {
                destinationArray[i] = sourceArray[k];
            }

            result = FromBitToNumberConverter(destinationArray, result);

            return result;
        }

        public static string FromNumberToBitConverter(int number)
        {
            int storeNumber = number;

            if (storeNumber < 0)
            {
                number = ~number;
            }

            StringBuilder builder = new StringBuilder();

            for (int i = 31; i >= 0; i--)
            {
                int checkValue = (int)Math.Pow(2, i);
                if (number - checkValue >= 0)
                {
                    builder.Append('1');
                    number -= checkValue;
                }
                else if (number - checkValue < 0)
                {
                    builder.Append('0');
                }
            }

            if (storeNumber < 0)
            {
                builder.Replace('1', '2');
                builder.Replace('0', '1');
                builder.Replace('2', '0');
            }

            return new string(builder.ToString());
        }

        public static int FromBitToNumberConverter(char[] destinationArray, int result)
        {
            if (destinationArray is null)
            {
                throw new ArgumentNullException(nameof(destinationArray));
            }

            int tempResult = 0;

            for (int i = 0; i < 32; i++)
            {
                if (destinationArray[i] == '1')
                {
                    tempResult += (int)Math.Pow(2, i);
                }
            }

            result = tempResult;

            return result;
        }
    }
}
